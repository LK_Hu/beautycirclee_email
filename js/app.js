App = Ember.Application.create();

App.Router.map(function() {
  // put your routes here
  this.route('about');
  this.route('team');
  this.route('mailinglist');
});

App.IndexRoute = Ember.Route.extend({
  model: function() {
    return ['red', 'yellow', 'blue'];
  }
});

// App.ApplicationView = Em.View.extend({
//   tagName: 'div',
//   // classNames: [],
//   didInsertElement: function() {
//     // this.$().backstretch("http://dl.dropbox.com/u/515046/www/garfield-interior.jpg", {centeredX: true, centeredY: true});
//   }
// });


