$(document).ready(function() {

  // MetsiMenu
  $('#side-menu').metisMenu();

  // Full height of sidebar
  function fix_height() {
      var heightWithoutNavbar = $("body > #wrapper").height() - 61;
      $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");
  }
  fix_height();

  $(window).bind("load resize click scroll", function() {
      if(!$("body").hasClass('body-small')) {
          fix_height();
      }
  });

  // minimalize menu
  $('.navbar-minimalize').click(function () {
      $("body").toggleClass("mini-navbar");
      SmoothlyMenu();
  });
});

// Minimalize menu when screen is less than 768px
$(function() {
    $(window).bind("load resize", function() {
        if ($(this).width() < 769) {
            $('body').addClass('body-small')
        } else {
            $('body').removeClass('body-small')
        }
    });
});

function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 100);
    } else if ($('body').hasClass('fixed-sidebar')){
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 300);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

(function($) {
  window.fnames = new Array(); 
  window.ftypes = new Array();
  fnames[0]='EMAIL';
  ftypes[0]='email';
  fnames[1]='NAME';
  ftypes[1]='text';
  fnames[3]='MMERGE3';
  ftypes[3]='text';
  fnames[4]='MMERGE4';
  ftypes[4]='text';
}(jQuery));

var $mcj = jQuery.noConflict(true);
